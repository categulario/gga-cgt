use std::fs::{File, create_dir_all};
use std::io::{self, Read, Write};
use std::time::SystemTime;
use std::path::Path;

#[macro_use]
extern crate clap;

use clap::{Arg, App};

use gga_cgt::{Config, Problem, sol::{Solution, Bin}, solve};

fn main() -> io::Result<()> {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("config")
               .short("c")
               .long("config")
               .value_name("FILE")
               .help("The config file with parameters for the algorithm")
               .required(true)
               .takes_value(true)
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("DIR")
                .help("A directory where output of every case will be written (if it doesn't exist will be created)")
                .required(true)
                .takes_value(true)
        )
        .arg(
            Arg::with_name("INSTANCES")
               .help("Filenames representing the different instances to solve")
               .required(true)
               .multiple(true)
        )
        .get_matches();

    let config_filename = matches.value_of("config").unwrap();
    let mut config_file = File::open(config_filename)?;
    let mut config_contents = String::new();

    config_file.read_to_string(&mut config_contents)?;

    let config: Config = toml::from_str(&config_contents).expect("Invalid format for config");
    let output_dir = matches.value_of("output").unwrap();

    create_dir_all(output_dir)?;

    let solutions: Vec<_> = matches.values_of("INSTANCES").unwrap().map(|instance_filename| {
        let mut instance_file = File::open(instance_filename).unwrap();
        let mut instance_contents = String::new();

        instance_file.read_to_string(&mut instance_contents).unwrap();

        let instance: Problem = toml::from_str(&instance_contents).expect("Invalid format for config");
        let now = SystemTime::now();
        let (stopped_reason, sol, weights) = solve(instance, config);
        let elapsed = now.elapsed().unwrap();

        (instance_filename, Solution {
            elapsed: elapsed.as_secs_f64(),
            stopped_reason,
            bin_fullness: sol.bin_fullness(),
            generation: sol.generation(),
            num_full_bins: sol.num_full_bins(),
            fullness_of_emptiest_bin: sol.fullness_of_emptiest_bin(),
            num_bins: sol.bins.len(),
            bins: sol.bins.into_iter().map(|b| Bin::from_internal(b, &weights)).collect(),
        })
    }).collect();

    for (filename, solution) in solutions {
        let filename = Path::new(filename).file_name().unwrap();

        let mut file = File::create(Path::new(output_dir).join(filename))?;
        file.write_all(toml::to_string(&solution).unwrap().as_bytes())?;
    }

    Ok(())
}
