//! # GGA-CGT
//!
//! GROUPING GENETIC ALGORITHM WITH CONTROLLED GENE TRANSMISSION FOR THE BIN
//! PACKING PROBLEM
//!
//! Author: Marcela Quiroz-Castellanos
//! qc.marcela@gmail.com
//! Tecnológico Nacional de México
//! Instituto Tecnológico de Ciudad Madero
//! División de Estudios de Posgrado e Investigación
//! Depto. de Sistemas y Computación
//!
//! The program excecutes GGA-CGT over a set instances using different configurations
//! given by the user. Each configuration represents an independent execution of the GA.
//!
//! Reference:
//! Quiroz-Castellanos, M., Cruz-Reyes, L., Torres-Jimenez, J.,
//! Gómez, C., Huacuja, H. J. F., & Alvim, A. C. (2015).
//! A grouping genetic algorithm with controlled gene transmission for
//! the bin packing problem. Computers & Operations Research, 55, 52-64.
//!
//! Input:
//! File "instances.txt" including the name of the BPP instances to be solved;
//! Files including the standard instances to be solve;
//! File "configurations.txt" including the parameter values for each experiment;
//!
//! Output:
//! A set of files "GGA-CGT_(i).txt" including the experimental results for each
//! configuration i in the input, stored in directory: Solutions_GGA-CGT;
//! If(save_bestSolution = 1) a set of files HGGA_S_(i)_instance.txt including the
//! obtained solution for each instance, for each configuration i, stored in directory: Details_GGA-CGT;
use std::cmp::Reverse;

use rand::{Rng, SeedableRng, seq::SliceRandom};
use serde::{Serialize, Deserialize};
use ahash::AHashSet as HashSet;

pub mod sol;
pub mod health;

use sol::{Individual, IBin, Index, StopReason};

/// Describes a set of parameter settings for using in solving problem instances
#[derive(Copy, Clone, Deserialize)]
pub struct Config {
    /// How many individuals to keep in every iteration
    pub population_size: usize,

    /// Maximum number of iterations of the algorithm
    pub max_generations: usize,

    /// Individuals to mutate, percentage
    pub n_m: f64,

    /// Individuals to be recombined, percentage
    pub n_c: f64,

    /// Rate used to calculate the number of bins to eliminate. This is for when
    /// the solution is not cloned.
    pub k_ncs: f64,

    /// Rate used to calculate the number of bins to eliminate. This is for when
    /// the solution is cloned.
    pub k_cs: f64,

    /// Individuals in the elite set, percentage
    pub b_size: f64,

    /// Maximum age for individual to be cloned
    pub life_span: usize,

    /// Random seed
    pub seed: u64,
}

/// Describes the properties of a problem to solve.
#[derive(Debug, Serialize, Deserialize)]
pub struct Problem {
    /// The capacity of each bin.
    pub bin_capacity: u64,

    /// Is there a known best solution?
    pub known_best_solution: Option<usize>,

    /// How many weights are there in this problem.
    pub num_weights: usize,

    /// The weights of the items to fit in the containers.
    pub weights: Vec<u64>,
}

/// Solves exactly one instance of the problem.
///
/// Frist item in the response indicates if the solution is optimal, second item
/// is the optimal solution if any or the best solution found.
pub fn solve(problem: Problem, config: Config) -> (StopReason, Individual, Vec<u64>) {
    let Problem {
        mut weights, bin_capacity, known_best_solution: _, num_weights: _,
    } = problem;
    let Config {
        population_size, max_generations, seed, b_size, n_c, n_m, k_cs, k_ncs,
        life_span,
    } = config;

    // Sort weights in descending order
    weights.sort_unstable_by_key(|&w| Reverse(w));

    let mut rng = mt19937::MT19937::seed_from_u64(seed);
    let (n_, l2) = lowerbound(&weights, bin_capacity);
    let (optimal_solution, mut population) = generate_initial_population(population_size, &weights, bin_capacity, n_, l2, &mut rng);

    if let Some(sol) = optimal_solution {
        (StopReason::OptimalFound, sol, weights)
    } else {
        for g in 0..max_generations {
            match generation(&mut population, g + 1, b_size, n_c, &mut rng, &weights, bin_capacity, l2, n_m, life_span, k_cs, k_ncs) {
                GenerationResult::Optimal(sol) => {
                    return (StopReason::OptimalFound, sol, weights);
                }
                GenerationResult::RepeatedFitness => {
                    return (StopReason::RepeatedFitness, find_best_solution(&population).clone(), weights);
                }
                GenerationResult::NoSolution => {}
            }
        }

        (StopReason::GenerationLimit, find_best_solution(&population).clone(), weights)
    }
}

/// To generate an initial population P of individuals with FF-ñ packing
/// heuristic.
///
/// Returns a tuple. If the first element is Some(individual) it means a
/// solution was found in this early stage. None otherwise.
///
/// The second element of the tuple is the generated population.
fn generate_initial_population<R>(
    size: usize, weights: &[u64], bin_capacity: u64, n_: usize, l2: usize,
    rng: &mut R,
) -> (Option<Individual>, Vec<Individual>)
where
    R: Rng,
{
    let population: Vec<_> = (0..size).map(|_| ff_n_(weights, bin_capacity, n_, rng)).collect();
    let optimal = population.iter().find(|s| s.bins.len() == l2).cloned();

    (optimal, population)
}

/// The result of each generation
pub enum GenerationResult {
    /// An optimal solution was found, must stop iterating.
    Optimal(Individual),

    /// More than 10% of the population has repeated fitness, must stop
    /// iterating.
    RepeatedFitness,

    /// Nothing interesting was found, must continue to the next generation.
    NoSolution,
}

pub use GenerationResult::Optimal;

/// Computes how many individuals have the same low repeated fitness.
///
/// *Invariants*
///
/// It assumes that the population is sorted ascending by its fitness.
fn compute_repeated_fitness(population: &[Individual]) -> usize {
    let fullness_of_worst_solution = population.first().unwrap().bin_fullness();

    population
        .iter()
        .map(|s| s.bin_fullness())
        .take_while(|f| *f == fullness_of_worst_solution)
        .count()
}

/// To apply the reproduction technique: Controlled selection and Controlled
/// replacement.
fn generation<R>(
    population: &mut [Individual], gen: usize, b_size: f64, n_c: f64, rng: &mut R,
    weights: &[u64], bin_capacity: u64, l2: usize, n_m: f64, life_span: usize,
    k_cs: f64, k_ncs: f64,
) -> GenerationResult
where
    R: Rng,
{
    //--------------------------------------------------------------------------
    //---------------------------------Controlled selection for crossover-------
    //--------------------------------------------------------------------------
    // sort population ascending by its fitness
    population.sort_unstable_by(|a, b| a.bin_fullness().partial_cmp(&b.bin_fullness()).unwrap());
    let repeated_fitness = compute_repeated_fitness(population);

    if gen > 1 && repeated_fitness > (0.1 * population.len() as f64) as usize {
        return GenerationResult::RepeatedFitness;
    }

    // a shuffled set of indices to the best individuals in the population
    let best_individuals = {
        let mut individuals: Vec<usize> = (
            (
                ((1.0 - n_c) * population.len() as f64) as usize
            )..population.len()
        ).collect();
        individuals.shuffle(rng);
        individuals.into_iter()
    };

    // a shuffled set of indices to a random portion of the population
    let mut random_individuals = {
        let mut individuals: Vec<usize> = (
            0..(
                ((1.0 - b_size) * population.len() as f64) as usize
            )
        ).collect();
        individuals.shuffle(rng);
        individuals.into_iter()
    };

    // How many children to generate through crossover
    let population_to_combine = (n_c * population.len() as f64) as usize;
    // Pairs of indices (best, random) where is warrantied that best != random
    let pairs: Vec<_> = best_individuals
        .map(|b| (b, random_individuals.find(|&r| r != b).unwrap()))
        .take(population_to_combine / 2)
        .collect();

    let children: Vec<_> = pairs.iter().map(|(b, r)| {
            let f1 = &population[*b];
            let f2 = &population[*r];

            // Here I tried to optimize using an array::IntoIter instead of vec
            // but that turned out to be measurably slower.
            vec![
                gene_level_crossover_ffd(f1, f2, weights, bin_capacity, gen),
                gene_level_crossover_ffd(f2, f1, weights, bin_capacity, gen),
            ]
        })
        .flatten()
        .collect();

    for child in children.iter() {
        if child.bins.len() == l2 {
            return GenerationResult::Optimal(child.clone());
        }
    }

    //--------------------------------------------------------------------------
    //---------------------------------Controlled replacement for crossover-----
    //--------------------------------------------------------------------------
    // Replace n_c/2 of the randomly chosen solutions from the population with
    // the new children.
    let mut first_children = children;
    let second_children = first_children.split_off(population_to_combine / 2);

    for (r, child) in pairs.into_iter().map(|(_b, r)| r).zip(first_children) {
        population[r] = child;
    }

    // Here I might have found a discrepancy with the article, which establishes
    // that if there are individuals with the same fitness they should be
    // replaced first, then the rest replace the worst. The original code
    // doesn't actually replace individuals with repeated fitness.
    let mut i = 0;
    for child in second_children {
        // this loop is to avoid replacen other children that were just inserted
        while population[i].generation() == gen {
            i += 1;
        }

        population[i] = child;
    }

    //--------------------------------------------------------------------------
    //--------------------------------Controlled selection for mutation---------
    //--------------------------------------------------------------------------
    population.sort_unstable_by(|a, b| a.bin_fullness().partial_cmp(&b.bin_fullness()).unwrap());

    // find the number of items with the same low fitness
    let repeated_fitness = compute_repeated_fitness(population);

    if gen > 1 && repeated_fitness > (0.1 * population.len() as f64) as usize {
        return GenerationResult::RepeatedFitness;
    }

    //--------------------------------------------------------------------------
    //----------------------------------Controlled replacement for mutation-----
    //--------------------------------------------------------------------------
    let mut j = 0;
    let mut i = population.len() - 1;

    while i > population.len() - (n_m * population.len() as f64) as usize {
        let (to_mutate, k) = if j < (population.len() as f64 * b_size) as usize && gen - population[i].generation() < life_span {
            population[j] = population[i].clone();

            j += 1;

            (&mut population[j], k_cs)
        } else {
            (&mut population[i], k_ncs)
        };

        adaptive_mutation_rp(to_mutate, k, gen, bin_capacity, rng, weights);

        if population[i].bins.len() == l2 {
            return GenerationResult::Optimal(population[i].clone());
        }

        i -= 1;
    }

    GenerationResult::NoSolution
}

fn order_bins<'a>(bin1: &'a IBin, bin2: &'a IBin) -> (&'a IBin, &'a IBin) {
    if bin1.fullness() > bin2.fullness() {
        (bin1, bin2)
    } else {
        (bin2, bin1)
    }
}

/// To recombine two parent solutions producing a child solution.
///
/// Input:
///
/// * `father_1`: the first parent
/// * `father_2`: the second parent
/// * `weights`: the ordered weights of the problem
/// * `bin_capacity`: The capacity of the bin in this problem
/// * `gen`: generation where this solution was created
///
/// # Invariants
///
/// The solutions given as parents must have their bins ordered by fullness in
/// descending order
fn gene_level_crossover_ffd(
    father_1: &Individual, father_2: &Individual, weights: &[u64], bin_capacity: u64,
    gen: usize,
) -> Individual {
    let mut child_bins = Vec::with_capacity(father_1.bins.len().max(father_2.bins.len()));
    let mut unused_items: HashSet<Index> = (0..weights.len()).map(|a| a.into()).collect();

    // The original code did a shuffle and then sort by fullness. I tried using
    // the same logic but got no better results than without doing it, so the
    // code didn't make it to this version

    // Here I made an important change. Assuming that, if a bin exists it is
    // because it contains an element, I removed the check for .fullness() > 0
    //
    // That change allows to see the symmetry of the if and else, from there it
    // was possible to imagine a function
    //
    // order(&bin1, &bin2) -> (&greater, &lower)
    //
    // that is used to remove the duplicated code
    for (f1bin, f2bin) in father_1.bins.iter().zip(father_2.bins.iter()) {
        let (greater, lower) = order_bins(f1bin, f2bin);

        if !used_items(greater, &child_bins) {
            child_bins.push(greater.clone());
            greater.remove_from_unused(&mut unused_items);
        }

        if !used_items(lower, &child_bins) {
            child_bins.push(lower.clone());
            lower.remove_from_unused(&mut unused_items);
        }
    }

    let mut sol = Individual::with_bins(child_bins, bin_capacity, gen);
    let mut bin_i = 0;

    for item in unused_items.into_iter() {
        ff(item, &mut sol, &mut bin_i, weights, bin_capacity);
    }

    sol.bins.sort_unstable_by_key(|b| Reverse(b.fullness()));

    sol
}

/// To produces a small modification in a solution.
///
/// Input:
/// * `individual`: The position in the population of the solution to mutate.
/// * `k`: The rate of change to calculate the number of bins to eliminate.
fn adaptive_mutation_rp<R>(
    individual: &mut Individual, k: f64, gen: usize, bin_capacity: u64, rng: &mut R,
    weights: &[u64],
)
where
    R: Rng,
{
    // Sort bins in descending order
    individual.bins.sort_unstable_by(|a, b| b.fullness().partial_cmp(&a.fullness()).unwrap());

    // The number of bins that are not full
    let nfb = individual.bins.iter().filter(|b| b.fullness() < bin_capacity).count();

    // The number of bins to be removed from the solution
    let _p_ = 1.0 / k;
    let number_bins = (
        nfb as f64 * (
            (2 - nfb / individual.bins.len()) as f64 / (nfb as f64).powf(_p_)
        ) * (
            1.0 - rng.gen_range(1..(((1.0 / (nfb as f64).powf(_p_)) * 100.0).ceil() as u64)) as f64 / 100.0
        )
    ).ceil() as usize;

    // Extract the specified bins from the solution
    let emptiest_bins = individual.bins.split_off(individual.bins.len() - number_bins);

    // Compute the set of free items from the removed bins
    let free_items: Vec<_> = IBin::free_items_from_bins(emptiest_bins);

    rp(individual, free_items, weights, rng, bin_capacity, gen);
}

/// To generate a random BPP solution with the ñ large items packed in separate
/// bins.
///
/// Input:
///
/// The position in the population of the new solution: individual
fn ff_n_<R>(weights: &[u64], bin_capacity: u64, n_: usize, rng: &mut R) -> Individual
where
    R: Rng,
{
    let mut bin_i = 0;

    let (mut sol, mut permutation): (_, Vec<Index>) = if n_ > 0 {
        let bins: Vec<_> = (0..n_).map(|i| IBin::with_item(i.into(), weights)).collect();

        (
            Individual::with_bins(bins, bin_capacity, 0),
            (0..weights.len()).skip(n_).map(|a| a.into()).collect::<Vec<_>>()
        )
    } else {
        (
            Individual::new(),
            (0..weights.len()).map(|a| a.into()).collect::<Vec<_>>()
        )
    };

    permutation.shuffle(rng);

    for item in permutation {
        ff(item, &mut sol, &mut bin_i, weights, bin_capacity);
    }

    sol.bins.sort_unstable_by_key(|b| Reverse(b.fullness()));

    sol
}

/// To reinsert free items into an incomplete BPP solution.
///
/// Input:
/// * `individual`: The incomplete solution where the free items must be
///   reinserted.
/// * `free_items`: A set of free items to be reinserted into the partial
///   solution.
fn rp<R>(
    individual: &mut Individual, mut free_items: Vec<Index>, weights: &[u64],
    rng: &mut R, bin_capacity: u64, gen: usize,
) where
    R: Rng,
{
    let mut bins = individual.bins.split_off(0);

    bins.shuffle(rng);
    free_items.shuffle(rng);
    let number_free_items = free_items.len();

    // iterate over available bins
    let bins = bins.into_iter().map(|bin| {
        let (fullness, items) = bin.into_fullness_and_items();

        let mut sum = fullness;
        let mut bin_weights = items;

        // For every weight in this container
        'outer: for p in 0..bin_weights.len() {
            // For every other weight in this container
            for s in (p + 1)..bin_weights.len() {
                // For every existing free item
                for k in 0..number_free_items {
                    let free_weight_k = weights[free_items[k].n()];
                    let weight_p = weights[bin_weights[p].n()];
                    let weight_s = weights[bin_weights[s].n()];
                    let old_pair_sum = weight_p + weight_s;

                    // In the original code the following conditional was inside
                    // the innermost loop and it was duplicated for k and k2.
                    // I studied the performance of both versions and found no
                    // improvement in run-time or in exploration of the solution
                    // space.

                    // Si el k-ésimo peso libre pesa más que los pesos de p
                    // y s juntos y al reemplazar ambos por éste caben
                    // bien...
                    if free_weight_k >= old_pair_sum &&
                        sum - old_pair_sum + free_weight_k <= bin_capacity
                    {
                        // Actualiza el peso
                        sum = sum - old_pair_sum + free_weight_k;

                        // Libera p y s
                        let new_free_items_0 = bin_weights[p];
                        let new_free_items_1 = bin_weights[s];

                        // Mete el peso libre en p
                        bin_weights[p] = free_items[k];

                        // Saca a s de este contenedor
                        bin_weights.remove(s);

                        // Añade los elementos removidos al elemento de libres
                        free_items[k] = new_free_items_0;
                        free_items.push(new_free_items_1);

                        break 'outer;
                    }

                    for k2 in (k+1)..number_free_items {
                        let free_weight_k = weights[free_items[k].n()];
                        let weight_p = weights[bin_weights[p].n()];
                        let weight_s = weights[bin_weights[s].n()];
                        let old_pair_sum = weight_p + weight_s;
                        let free_weight_k2 = weights[free_items[k2].n()];
                        let free_pair_sum = free_weight_k + free_weight_k2;

                        if free_pair_sum > old_pair_sum ||
                            (
                                // o pesa igual pero son diferentes elementos
                                (free_pair_sum == old_pair_sum) &&
                                    !(free_weight_k == weight_p || free_weight_k == weight_s)
                            )
                        {
                            if sum - old_pair_sum + free_pair_sum > bin_capacity {
                                break;
                            }

                            sum = sum - old_pair_sum + free_pair_sum;
                            let new_free_items_0 = bin_weights[p];
                            let new_free_items_1 = bin_weights[s];
                            bin_weights[p] = free_items[k];
                            bin_weights[s] = free_items[k2];
                            free_items[k] = new_free_items_0;
                            free_items[k2] = new_free_items_1;

                            if sum == bin_capacity {
                                break 'outer;
                            }
                        }
                    }
                } // for loop over free items
            } // loop over items of this bin from p
        } // loop over items of this bin

        IBin::with_items(bin_weights, sum)
    }); // for loop over bins

    individual.bins.extend(bins);

    // Recompute all of individual's invariants

    // TODO compute a good value for bin_i, which perhaps can be computed with
    // the individual invariants
    let mut bin_i = 0;

    // Sort free items in ascending order
    free_items.sort_by_key(|&f| weights[f.n()]);

    let higher_weight = weights[free_items.last().unwrap().n()];
    let lighter_weight = weights[free_items.first().unwrap().n()];

    if higher_weight < bin_capacity / 2 {
        free_items.shuffle(rng);
    }

    individual.recompute_invariants(bin_capacity);
    individual.bins.sort_unstable_by_key(|b| Reverse(b.fullness()));

    if lighter_weight > bin_capacity - individual.fullness_of_emptiest_bin() {
        bin_i = individual.bins.len();
    }

    // reacomodate missing items
    for item in free_items {
        ff(item, individual, &mut bin_i, weights, bin_capacity);
    }

    individual.set_generation(gen);
}

/// To insert an item into an incomplete BPP solution.
///
/// Input:
///
/// * item: The index of the weight to be inserted in the solution
/// * individual: An incomplete chromosome where the item must be inserted
/// * beginning: The first bin that could have sufficient available capacity to
///   store the item
fn ff(item: Index, individual: &mut Individual, beginning: &mut usize, weights: &[u64], bin_capacity: u64) {
    let pos = if weights[item.n()] > (bin_capacity - individual.fullness_of_emptiest_bin()) {
        None
    } else {
        individual.bins
            .iter()
            .enumerate()
            .skip(*beginning)
            .find(|(_i, b)| b.fullness() + weights[item.n()] <= bin_capacity)
            .map(|(i, _b)| i)
    };

    if let Some(i) = pos {
        individual.bins[i].add_item(item, weights);

        // if this bin's contents plus the smallest weight exceeds the bin
        // capacity update `beginning` which points to the first bin with enough
        // space for another item
        if individual.bins[i].fullness() + weights.last().unwrap() > bin_capacity && i == *beginning {
            *beginning += 1;
        }
    } else {
        individual.bins.push(IBin::with_item(item, weights));
    }

    // some invariants could be kept in this function, as it was in the original
    // code, but the solution's bin_fullness needs to account for the situation
    // where an item is added to an existing bin, thus the only clean solution
    // is to recompute it completely. Therefore we might as well just recompute
    // all invariants in the same loop.
    individual.recompute_invariants(bin_capacity);
}

/// To calculate the lower bound L2 of Martello and Toth and the ñ large items n_
fn lowerbound(weights: &[u64], bin_capacity: u64) -> (usize, usize) {
    let total_accumulated_weight: u64 = weights.iter().sum();
    let jx = weights.iter().filter(|&w| *w > bin_capacity / 2).count();
    let n_ = jx;

    if jx == weights.len() {
        return (n_, jx);
    }

    (n_, if jx == 0 {
        // TODO roughly translated, must check for accuracy
        if total_accumulated_weight % bin_capacity >= 1 {
            (total_accumulated_weight as f64 / bin_capacity as f64).ceil() as usize
        } else {
            (total_accumulated_weight / bin_capacity) as usize
        }
    } else {
        let cj12 = jx;
        let sjx: u64 = weights[jx..].iter().sum();

        let mut jp: usize = weights[..jx]
            .iter()
            .enumerate()
            .find(|(_i, &w)| w <= bin_capacity - weights[jx])
            .map(|(i, _w)| i)
            .unwrap_or(jx);

        let mut cj2 = jx - jp;
        let mut sj2: u64 = weights[jp..jx].iter().sum();
        let mut jpp = jx;
        let mut sj3: u64 = weights.iter().filter(|w| **w == weights[jpp]).sum();
        let mut l2 = cj12;

        loop {
            let aux1 = if (sj3 + sj2) % bin_capacity >= 1 {
                ((sj3 + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)).ceil() as usize
            } else {
                ((sj3 + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)) as usize
            };

            if l2 < (cj12 + aux1) {
                l2 = cj12 + aux1;
            }

            jpp += 1;

            if jpp < weights.len() {
                sj3 += weights[jpp];

                while jpp+1 < weights.len() && weights[jpp+1] == weights[jpp] {
                    jpp += 1;
                    sj3 += weights[jpp];
                }

                while jp > 0 && weights[jp-1] <= bin_capacity - weights[jpp] {
                    jp -= 1;
                    cj2 += 1;
                    sj2 += weights[jp];
                }
            }

            let aux2 = if (sjx + sj2) % bin_capacity >= 1 {
                ((sjx + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)).ceil() as usize
            } else {
                ((sjx + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)) as usize
            };

            if !(jpp <= weights.len() || (cj12 + aux2) > l2) {
                break;
            }
        }

        l2
    })
}

/// To find the solution with the highest fitness of the population and update the global_best_solution
fn find_best_solution(population: &[Individual]) -> &Individual {
    population
        .iter()
        .max_by(|a, b| a.bin_fullness().partial_cmp(&b.bin_fullness()).unwrap())
        .unwrap()
}

/// To check if any of the items of the current bin is already in the solution
///
/// Input:
/// The bin to check
/// An array that indicates the items that are already in the solution: items
///
/// Output:
/// (true) when some of the items in the current bin is already in the solution
/// (false) otherwise
fn used_items(bin: &IBin, used: &[IBin]) -> bool {
    used.iter().any(|b| b.intersects(bin))
}

#[cfg(test)]
mod tests {
    use std::cmp::Reverse;

    use rand::SeedableRng;

    use super::{adaptive_mutation_rp, ff_n_, lowerbound};
    use crate::{health::validates, sol::Solution};

    #[test]
    fn test_mutation() {
        let mut weights = vec![34978, 34849, 34703, 34608, 34598, 34524, 34356, 34308, 34069, 34049, 33895, 33842, 33806, 33738, 33716, 33590, 33546, 33507, 33468, 33465, 33383, 33190, 33075, 32976, 32897, 32762, 32696, 32638, 32553, 32398, 32230, 32176, 31967, 31954, 31903, 31782, 31724, 31686, 31597, 31561, 31532, 31499, 31346, 30943, 30915, 30869, 30766, 30683, 30678, 30644, 30559, 30448, 30315, 30238, 30125, 29974, 29947, 29890, 29886, 29858, 29856, 29783, 29697, 29438, 29427, 29301, 29174, 29173, 29123, 29117, 29116, 29095, 29094, 29063, 29041, 29038, 28977, 28946, 28921, 28910, 28842, 28703, 28360, 28350, 28305, 28302, 28225, 28160, 28094, 28040, 28020, 27901, 27775, 27765, 27688, 27439, 27425, 27394, 27365, 27349, 27284, 27180, 26935, 26881, 26867, 26795, 26703, 26651, 26550, 26432, 26375, 26368, 26244, 26204, 26192, 26181, 26158, 26133, 26067, 25945, 25906, 25759, 25698, 25688, 25652, 25615, 25530, 25528, 25366, 25324, 25273, 25142, 24852, 24846, 24658, 24592, 24564, 24463, 24457, 24374, 24359, 24332, 23987, 23956, 23952, 23932, 23895, 23837, 23795, 23774, 23663, 23621, 23502, 23453, 23430, 23366, 23178, 23090, 22991, 22942, 22743, 22442, 22432, 22415, 22338, 22134, 22081, 22014, 21950, 21948, 21796, 21784, 21727, 21722, 21557, 21498, 21480, 21315, 21193, 21127, 21060, 20997, 20837, 20813, 20693, 20693, 20686, 20677, 20676, 20664, 20663, 20634, 20616, 20570, 20566, 20496, 20441, 20307, 20226, 20114];

        weights.sort_unstable_by_key(|&w| Reverse(w));

        let bin_capacity = 100000;
        let (n_, _l2) = lowerbound(&weights, bin_capacity);
        let mut rng = mt19937::MT19937::seed_from_u64(1);
        let mut individual = ff_n_(&weights, bin_capacity, n_, &mut rng);

        adaptive_mutation_rp(&mut individual, 2.0, 1, bin_capacity, &mut rng, &weights);

        validates(Solution::wraps(individual, &weights), bin_capacity, &weights).unwrap();
    }
}
