use std::collections::HashSet;
use std::fmt;

use crate::sol::{Bin, Solution};

#[derive(Debug)]
pub enum ValidationResult {
    /// Duplicated weight at bin with given index
    DuplicatedWeight(usize, u64),

    /// The reported fullness and the computed fullness for this bin don't match
    MismatchingBinFullness {
        bin_weights: Vec<u64>,
        given: u64,
        computed: u64,
    },

    /// Some items are missing allocation
    UnallocatedItems(Vec<(usize, u64)>),

    /// Bin has more items than the capacity allows
    ExceededCapacity(Bin),

    /// The weight and index of a bin don't match.
    MismatchingWeight(Bin, usize),
}

impl fmt::Display for ValidationResult {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ValidationResult::DuplicatedWeight(i, w) => write!(formatter, "Duplicated weight {} with index {}", w, i),
            ValidationResult::MismatchingBinFullness{ bin_weights, given, computed, } => write!(
                formatter,
                "Mismatching bin fullness: weights: {:?} computed: {} given: {}", bin_weights, computed, given,
            ),
            ValidationResult::UnallocatedItems(items) => write!(formatter, "Unallocated items: {:?}", items),
            ValidationResult::ExceededCapacity(bin) => write!(formatter, "Exceeded capacity: {:?}", bin),
            ValidationResult::MismatchingWeight(bin, i) => write!(formatter, "Mismatching bin weight at index: {} bin: {:?}", i, bin),
        }
    }
}

impl std::error::Error for ValidationResult {
}

/// Checks if a solution is valid
pub fn validates(solution: Solution, bin_capacity: u64, weights: &[u64]) -> Result<(), ValidationResult> {
    let mut used_weights = HashSet::new();

    for bin in solution.bins {
        let mut bin_weight = 0;
        let mut bin_weights = Vec::new();

        for item in &bin.items {
            if item.weight != weights[item.item] {
                return Err(ValidationResult::MismatchingWeight(bin.clone(), item.item));
            }

            if !used_weights.insert(item.item) {
                return Err(ValidationResult::DuplicatedWeight(item.item, item.weight));
            }

            bin_weights.push(item.weight);

            bin_weight += item.weight;
        }

        if bin_weight > bin_capacity {
            return Err(ValidationResult::ExceededCapacity(bin.clone()));
        }

        if bin_weight != bin.fullness {
            return Err(ValidationResult::MismatchingBinFullness{
                bin_weights,
                given: bin.fullness,
                computed: bin_weight,
            });
        }
    }

    if used_weights.len() != weights.len() {
        let missing: Vec<_> = weights.iter().cloned().enumerate().filter(|(i, _w)| {
            !used_weights.contains(i)
        }).collect();

        return Err(ValidationResult::UnallocatedItems(missing));
    }

    Ok(())
}
