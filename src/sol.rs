use serde::{Serialize, Deserialize};
use ahash::AHashSet as HashSet;

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Index(u16);

impl From<usize> for Index {
    fn from(t: usize) -> Index {
        Index(t as u16)
    }
}

impl Index {
    pub fn new(i: usize) -> Index {
        Index(i as u16)
    }

    /// Returns the index as a usize, to be used for indexing in slices.
    pub fn n(&self) -> usize {
        self.0 as usize
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct BinItem {
    pub item: usize,
    pub weight: u64,
}

impl BinItem {
    pub fn with_item_and_weight(item: usize, weight: u64) -> BinItem {
        BinItem {
            item, weight,
        }
    }
}

/// A container.
///
/// This structure is meant for reading and writing to output formats.
/// Internally the structure used is [`IBin`] due to the requirements and
/// optimizations of the algorithm.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Bin {
    /// The sum of the items in this container
    pub fullness: u64,

    /// The items this bin contains
    pub items: Vec<BinItem>,
}

impl Bin {
    pub fn from_internal(bin: IBin, weights: &[u64]) -> Bin {
        Bin {
            fullness: bin.fullness,
            items: bin.items.into_iter().map(|i| BinItem::with_item_and_weight(i.n(), weights[i.n()])).collect(),
        }
    }
}

/// A container.
///
/// This struct is used internally to manage the items of a container. It is
/// subject to changes due to optimizations.
#[derive(Clone, Debug)]
pub struct IBin {
    /// Occupancy of this bin
    fullness: u64,

    /// Indices in the weights array to the items contained in this bin
    items: Vec<Index>,
}

impl IBin {
    /// Creates a new bin with a single item
    pub fn with_item(item: Index, weights: &[u64]) -> IBin {
        IBin {
            items: vec![item],
            fullness: weights[item.n()],
        }
    }

    /// Creates a new bin from its items and precomputed fullness
    pub fn with_items(items: Vec<Index>, fullness: u64) -> IBin {
        IBin {
            fullness,
            items,
        }
    }

    /// Adds an item to this bin, keeping the invariant of its fullness
    pub fn add_item(&mut self, item: Index, weights: &[u64]) {
        self.fullness += weights[item.n()];
        self.items.push(item);
    }

    pub fn fullness(&self) -> u64 {
        self.fullness
    }

    /// Checks wether the contents of the bin intersect the items given
    pub fn intersects(&self, other: &IBin) -> bool {
        self.items.iter().find(|i| other.items.contains(i)).is_some()
    }

    pub fn remove_from_unused(&self, unused_items: &mut HashSet<Index>) {
        for item in self.items.iter() {
            unused_items.remove(&item);
        }
    }

    pub fn items(&self) -> impl Iterator<Item = &Index> {
        self.items.iter()
    }

    pub fn into_items(self) -> Vec<Index> {
        self.items
    }

    pub fn into_fullness_and_items(self) -> (u64, Vec<Index>) {
        (self.fullness, self.items)
    }

    /// Given a set of bins returns the indices that are not in any of them.
    /// `len` is the amount of weights of the current BPP instance being solved
    pub fn free_items_from_bins(bins: Vec<IBin>) -> Vec<Index> {
        bins.into_iter().map(|b| b.into_items()).flatten().collect()
    }
}

#[derive(Clone, Default)]
pub struct Individual {
    /// Saves the fitness of the solution i
    bin_fullness: f64,

    /// Saves the generation in which the solution i was generated
    generation: usize,

    /// Saves the number of bins in the solution i that are fully at 100%
    num_full_bins: usize,

    /// Saves the fullness of the bin with the highest avaliable capacity in the solution i
    fullness_of_emptiest_bin: u64,

    /// the bins that this solution defines
    pub bins: Vec<IBin>,

}

impl Individual {
    /// Create a new empty solution
    pub fn new() -> Individual {
        Default::default()
    }

    /// Create a new solution with the given bins.
    ///
    /// this method ensures the invariants of a solution are kept.
    pub fn with_bins(bins: Vec<IBin>, bin_capacity: u64, generation: usize) -> Individual {
        let mut sol = Individual {
            bins,
            generation,
            ..Default::default()
        };

        sol.recompute_invariants(bin_capacity);

        sol
    }

    /// Keep the solution's invariants
    ///
    /// * update the number of full bins
    /// * recompute solution fitness (bin_fullness)
    /// * recompute fullness_of_emptiest_bin
    pub fn recompute_invariants(&mut self, bin_capacity: u64) {
        let (num_full_bins, bin_fullness, fullness_of_emptiest_bin) = self.bins.iter()
            .fold((0, 0.0, u64::MAX), |acc, b| {
                let (mut num_full_bins, mut bin_fullness, mut fullness_of_emptiest_bin) = acc;

                if b.fullness == bin_capacity {
                    num_full_bins += 1;
                }

                bin_fullness += (b.fullness as f64 / bin_capacity as f64).powi(2);

                if b.fullness < fullness_of_emptiest_bin {
                    fullness_of_emptiest_bin = b.fullness;
                }

                (num_full_bins, bin_fullness, fullness_of_emptiest_bin)
            });

        self.num_full_bins = num_full_bins;
        self.bin_fullness = bin_fullness / self.bins.len() as f64;
        self.fullness_of_emptiest_bin = fullness_of_emptiest_bin;
    }

    pub fn num_full_bins(&self) -> usize {
        self.num_full_bins
    }

    /// Returns this solution's fullness_of_emptiest_bin value
    pub fn fullness_of_emptiest_bin(&self) -> u64 {
        self.fullness_of_emptiest_bin
    }

    pub fn bin_fullness(&self) -> f64 {
        self.bin_fullness
    }

    pub fn generation(&self) -> usize {
        self.generation
    }

    pub fn set_generation(&mut self, gen: usize) {
        self.generation = gen;
    }
}

/// An enum that describes why the algorithm stopped.
#[derive(Debug, Serialize, Deserialize)]
pub enum StopReason {
    /// An individual meeting the L2 bound was found.
    OptimalFound,

    /// More than some percentage of the individuals reported exactly the same
    /// fitness.
    RepeatedFitness,

    /// The maximum number of generations was reached.
    GenerationLimit,
}

#[derive(Serialize, Deserialize)]
pub struct Solution {
    /// Seconds that took to compute the solution
    pub elapsed: f64,

    /// Why did the algorithm stop? There are at least three possible reasons
    /// for that.
    pub stopped_reason: StopReason,

    /// Saves the fitness of the solution i
    pub bin_fullness: f64,

    /// Saves the generation in which the solution i was generated
    pub generation: usize,

    /// Saves the number of bins in the solution i that are fully at 100%
    pub num_full_bins: usize,

    /// Saves the fullness of the bin with the highest avaliable capacity in the solution i
    pub fullness_of_emptiest_bin: u64,

    /// The number of bins this solution contains
    pub num_bins: usize,

    /// The bins of this solution
    pub bins: Vec<Bin>,
}

impl Solution {
    /// Easily wrap an individual into a solution. Mostly for testing.
    pub fn wraps(individual: Individual, weights: &[u64]) -> Solution {
        Solution {
            elapsed: 0.0,
            bin_fullness: individual.bin_fullness,
            generation: individual.generation,
            num_full_bins: individual.num_full_bins,
            fullness_of_emptiest_bin: individual.fullness_of_emptiest_bin,
            num_bins: individual.bins.len(),
            bins: individual.bins.into_iter().map(|b| Bin::from_internal(b, weights)).collect(),
            stopped_reason: StopReason::OptimalFound,
        }
    }
}
