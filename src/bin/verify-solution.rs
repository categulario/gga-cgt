use std::fs::File;
use std::io::{self, Read};
use std::cmp::Reverse;

#[macro_use]
extern crate clap;

use clap::{Arg, App};

use gga_cgt::{Problem, sol::Solution, health::validates};

fn parse_problem(filename: &str) -> io::Result<Problem> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    Ok(toml::from_str(&contents).expect("Invalid format for config"))
}

fn parse_solution(filename: &str) -> io::Result<Solution> {
    let mut file = File::open(filename)?;
    let mut contents = String::new();

    file.read_to_string(&mut contents)?;

    Ok(toml::from_str(&contents).expect("Invalid format for config"))
}

fn main() -> io::Result<()> {
    let matches = App::new("verify-solution")
        .version(crate_version!())
        .author(crate_authors!())
        .about("Checks if a solution of the BPP is correct")
        .arg(
            Arg::with_name("problem")
               .short("p")
               .long("problem")
               .value_name("PROBLEM")
               .help("The problem description file")
               .required(true)
               .takes_value(true)
        )
        .arg(
            Arg::with_name("solution")
               .short("s")
               .long("solution")
               .value_name("SOLUTION")
               .help("A solution to the given problem")
               .required(true)
               .takes_value(true)
        )
        .get_matches();

    let problem = parse_problem(matches.value_of("problem").unwrap())?;
    let solution = parse_solution(matches.value_of("solution").unwrap())?;

    let Problem {
        bin_capacity, known_best_solution, mut weights, num_weights,
    } = problem;

    weights.sort_unstable_by_key(|&w| Reverse(w));

    println!("-- Problem --");
    println!("");
    println!("bin_capacity:        {}", bin_capacity);
    println!("known_best_solution: {:?}", known_best_solution);
    println!("Number of weights:   {}", num_weights);
    println!("");
    println!("-- Solution --");
    println!("");
    println!("Time:                       {}", solution.elapsed);
    println!("Why did the algorithm stop: {:?}", solution.stopped_reason);
    println!("Bins:                       {}", solution.bins.len());
    println!("Fullness:                   {}", solution.bin_fullness);
    println!("Generation:                 {}", solution.generation);
    println!("Num full bins:              {}", solution.num_full_bins);
    println!("Fullness of emptiest bin:   {}", solution.fullness_of_emptiest_bin);
    println!("");
    println!("-- Veredict --");
    println!("");

    if let Err(e) = validates(solution, bin_capacity, &weights) {
        println!("{}", e);
    } else {
        println!("Correct");
    }

    Ok(())
}
