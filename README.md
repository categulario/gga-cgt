# GGA-CGT

Grouping Genetic Algorithm with Controlled Gene Transmission for the bin packing
problem.

I didn't write the algorithm, I'm porting it to the Rust programming language.

## C version

Enter the `reference/` directory, the source is in `GGA-CGT.cpp`. Build with
make:

    make debug

or

    make main

and run the generated executable:

    ./main

## Rust version

Compile the release version:

    cargo build --release

and run with some options:

    target/release/gga-cgt -c sample/config.toml -o output instances/Hard0.toml

or see some help

    target/release/gga-cgt --help

## Profiling tools

Using [perf](https://perf.wiki.kernel.org/index.php/Tutorial):

    perf stat -e cycles,instructions,cache-misses target/release/gga-cgt -c sample/config.toml -o output instances/Hard0.toml

Getting a [flamegraph](https://github.com/flamegraph-rs/flamegraph)

    flamegraph target/release/gga-cgt -c sample/config.toml -o output instances/Hard0.toml

Comparing programs with hyperfine

    hyperfine 'prog1 args..' 'prog2 args...'

Callgrind

    valgrind --tool=callgrind --dump-instr=yes --collect-jumps=yes program [and args]
